.PHONY: dev

deps:
	go get github.com/go-chi/chi/v5
	go get github.com/go-chi/cors
	
dev:
	@go run ./cmd/main.go