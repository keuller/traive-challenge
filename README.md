# traive-challenge

## Transaction API

This API consists in 2 features:

- save transaction
- list transaction (internal useo only)

## Tech Stack

- Language: Golang v1.22
- Database: PostgreSQL 16
- Messaging: RabbitMQ
- Execution Runtime: Docker

## Technical Decisions

- Golang: This a given from the challenge.
  - The code is structured based on domain and separation of concern
  - The code standard focus on function based strategy rather than pseudo-OOP
  - The unit tests tries to focus more a BDD style to be more description than imperative
- PostgreSQL as database, was chosen based on a few criteria:
  - Its wide support for many cloud providers
  - SQL ANSI compliant
  - Stable, mature, reliable and performant
  - Used world wide as main database solution by big companies
  - Solid and vast documentation
  - Great support by Golang community
- RabbitMQ as message broker to implement notification sub-system
  - Solid and vast documentation
  - Stable, mature, reliable and performant
  - Great support by Golang community
- Docker as a runtime to run all components in a simple way as isolated container

## Data Model

- Account

| Field      | Data Type | Description                            |
| ---------- | --------- | -------------------------------------- |
| ID         | `string`  | Based on Nanoid                        |
| Owner      | `string`  | Based on Nanoid                        |
| Balance    | `float`   | Current balance                        |
| Limit      | `float`   | Account limit (e.g. )                  |
| Updated At | `time`    | date/time last update based on RFC3339 |

- Transaction

| Field      | Data Type | Description                            |
| ---------- | --------- | -------------------------------------- |
| ID         | `string`  | Based on nanoid                        |
| AccountID  | `string`  | Based on nanoid                        |
| UserID     | `string`  | Based on nanoid                        |
| Origin     | `int`     | 1-desktop, 2-ios, 3-andoid             |
| Operation  | `int`     | 1-credit, 2-debit                      |
| Amount     | `float`   | monetary amount                        |
| Created At | `time`    | data/time of creation based on RFC3339 |

## Project Structure

The project is structured based on domain/business logic.

- `app`: contain the HTTP server runtime
- `cmd`: main command to run the application
- `internal`: contains the internal packages used by the application.
  - `config`: loads the environment variables used by the app

## Running Locally

## Running Development mode

```shell
make dev
```

> Downloading/Resolving project dependencies

```shell
make deps
```
