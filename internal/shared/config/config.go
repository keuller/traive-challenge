package config

import (
	"fmt"
	"os"
)

type AppConfig struct {
	Host string
	Port string
}

func (a AppConfig) ServerAddress() string {
	return fmt.Sprintf("%s:%s", a.Host, a.Port)
}

func GetAppConfig() *AppConfig {
	var host, port string
	var ok bool

	if host, ok = os.LookupEnv("APP_HOST"); !ok {
		host = "localhost"
	}

	if port, ok = os.LookupEnv("APP_PORT"); !ok {
		port = "3000"
	}

	return &AppConfig{host, port}
}
