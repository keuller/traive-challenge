package main

import "gitlab.com/keuller/traive-ac/app"

func main() {
	server := app.New()

	go server.Start()

	server.WaitForShutdown()
}
