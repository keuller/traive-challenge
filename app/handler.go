package app

import "net/http"

func defaultHandler(res http.ResponseWriter, req *http.Request) {
	res.WriteHeader(http.StatusOK)
	res.Write([]byte("Transaction API"))
}
