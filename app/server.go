package app

import (
	"context"
	"errors"
	"log"
	"log/slog"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"
	"gitlab.com/keuller/traive-ac/internal/shared/config"
)

const (
	Timeout = 15 * time.Second
)

type App struct {
	cfg    *config.AppConfig
	server *http.Server
}

func New() *App {
	app := &App{}

	app.cfg = config.GetAppConfig()
	app.initServer()
	app.initRoutes()

	return app
}

func (a *App) Start() {
	slog.Info("Transaction API running at http://" + a.cfg.ServerAddress())
	err := a.server.ListenAndServe()
	if err != nil && !errors.Is(err, http.ErrServerClosed) {
		log.Fatal("Cannot initiate server - " + err.Error())
	}
}

func (a *App) WaitForShutdown() {
	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt, os.Kill, syscall.SIGTERM)
	<-ctx.Done()

	slog.Info("Server is shutting down...")
	// stop receiving any request
	if err := a.server.Shutdown(context.Background()); err != nil {
		log.Fatal("Fail on stop the server.")
	}

	slog.Info("Server has been stopped.")
	stop()
}

// initialize defaul server parameters
func (a *App) initServer() {
	a.server = &http.Server{
		Addr:         a.cfg.ServerAddress(),
		IdleTimeout:  Timeout,
		ReadTimeout:  Timeout,
		WriteTimeout: Timeout,
	}
}

// initiates all application routes
func (a *App) initRoutes() {
	router := chi.NewRouter()
	a.applyMiddleware(router)

	router.Get("/", defaultHandler)
	a.server.Handler = router
}

// register all handler middlewares
func (a *App) applyMiddleware(router *chi.Mux) {
	router.Use(middleware.Recoverer)
	router.Use(middleware.StripSlashes)
	router.Use(middleware.Timeout(Timeout))
	router.Use(middleware.Heartbeat("/health"))
	router.Use(cors.Handler(cors.Options{
		AllowedOrigins:   []string{"http://*"},
		AllowedMethods:   []string{"GET", "POST", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type"},
		AllowCredentials: false,
	}))
}
